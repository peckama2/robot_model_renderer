#include <OgrePixelFormat.h>

namespace robot_model_renderer
{
  int ogrePixelFormatToCvMatType(const Ogre::PixelFormat& pf);
}